package com.example;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import com.google.common.base.Splitter;
import com.google.common.collect.Interner;

/**
 * Created by Roman Ugolnikov on 24-Nov-15.
 */
public class Utils {

    private static Properties prop = new Properties();
    private static InputStream input = null;

    public static Logger logger = Logger.getLogger(Utils.class.getName());

    public static String url;
    public static Map<String, String> users = new HashMap<>();

    public static boolean checkAllPosts;
    public  static int MAX_AMOUNT;


    private Utils() {
    }


    public static void loadUsers() {
        logger.info("start loading users");
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);

            url = prop.getProperty("facebook.url");
            MAX_AMOUNT = new Integer(prop.getProperty("max.count"));
            users = Splitter.on(",").withKeyValueSeparator("=").split(prop.getProperty("users.map"));
            checkAllPosts = prop.getProperty("check.all.posts").equalsIgnoreCase("true");
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info(users.size() + " users loaded");
    }

}
