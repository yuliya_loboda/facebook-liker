package com.example;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.logging.Logs;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DemoApplicationTests {

    public static final int DEFAULT_WAIT_TIME = 10;
    public static final String XPATH_LIKE_BUTTON = ".//a[@aria-pressed='false']/span[contains(.,'Нравится')]";
    public static final String XPATH_DISLIKE_BUTTON = ".//a[@aria-pressed='true']/span[contains(.,'Нравится')]";

    public static WebDriver driver;

    public static WebDriver getDriver() {
        return driver;
    }

    public static void setDriver(final WebDriver driver) {
        DemoApplicationTests.driver = driver;
    }

    @BeforeClass
    public static void setUp() {
        Utils.loadUsers();
    }

    /**
     * Setup the browser to use in the test.
     */
    @Before
    public void setUpTest() {
        DesiredCapabilities capabilities;

        capabilities = DesiredCapabilities.firefox();

        FirefoxProfile profile = new FirefoxProfile();
        profile.setEnableNativeEvents(true);

        // TODO: set accept language for firefox.

        capabilities.setCapability(FirefoxDriver.PROFILE, profile);
        setDriver(new FirefoxDriver(capabilities));

        capabilities.setJavascriptEnabled(true);
        capabilities.setCapability(CapabilityType.TAKES_SCREENSHOT, Boolean.TRUE);

        // Browser loglevel doesn't work for firefox ?
        LoggingPreferences prefs = new LoggingPreferences();
        prefs.enable(LogType.BROWSER, Level.WARNING);
        prefs.enable(LogType.DRIVER, Level.INFO);
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, prefs);

        WebDriver.Options options = getDriver().manage();
        WebDriver.Window window = options.window();

        // Make sure all elements are visible.
        driver.manage().window().maximize();
    }


    @After
    public void tearDown() {
        Logs logs = getDriver().manage().logs();

        driver.manage().deleteAllCookies();
        getDriver().close();
        getDriver().quit();
    }

    /**
     * @param locator locator
     */
    public void scrollDownToElement(By locator) {
        WebElement element = findElement(locator);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public void scrollDownToElement(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public void scrollDownOnePage(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    /**
     * @return WebElement
     * @throws org.openqa.selenium.NoSuchElementException
     */
    public WebElement findElement(By locator) {
        pauseUntilFound(locator, DEFAULT_WAIT_TIME);
        return getDriver().findElement(locator);
    }

    public void pauseUntilFound(By locator, long seconds) {
        new WebDriverWait(getDriver(), seconds).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void pauseUntilVisible(By locator, long seconds) {
        new WebDriverWait(getDriver(), seconds).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    public void pauseUntilFoundAll(By locator, long seconds) {

        new WebDriverWait(getDriver(), seconds).until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
    }

    public void pause(long seconds) {

        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param locator xpath to element
     * @return Element present on the page or not
     */
    public boolean isElementsExists(By locator) {
        return findWebElements(locator).size() != 0;
    }

    /**
     * @return WebElements
     */
    public List<WebElement> findWebElements(By locator) {
        List<WebElement> list = driver.findElements(locator);
        return list;
    }


    @Test
    public void likeAllPosts() {
        System.out.println("------------------------------------------------------------------------------------------");
        System.out.println("------------------------------------------------------------------------------------------");
        System.out.println();

        final Map<String, String> usersStatus = new HashMap<>();
        Utils.users.forEach((name, password) -> {
            //login
            getDriver().get(Utils.url);
            findElement(By.xpath(".//*[@id='email']")).clear();
            findElement(By.xpath(".//*[@id='email']")).sendKeys(name);
            findElement(By.xpath(".//*[@id='pass']")).clear();
            findElement(By.xpath(".//*[@id='pass']")).sendKeys(password);
            findElement(By.xpath(".//*[@id='loginbutton']")).click();

            if (isElementsExists(By.xpath(".//*[@data-click='profile_icon']"))) {

                usersStatus.put(name, "logged in");
                List<WebElement> likedPosts = new ArrayList<WebElement>();
                List<WebElement> notLikedPosts = new ArrayList<WebElement>();
                int likedPostsCount = 0;
                int likedPostsTotal = 0;
                boolean endReached = false;
                //for liked posts > 3
                while (!endReached) {
//                    if (findElement(By.xpath("//span[contains(.,'Дата рождения')]")).isDisplayed()) {
//                    if (findElement(By.xpath("//span[contains(.,'Родился')]")).isDisplayed()) {
//                    if (findElement(By.xpath("//a[@data-key='year_1951']")).getAttribute("class").length() > 5) { //_3d5b _-fk = 8 VS _3d5b = 5
                    if (findWebElements(By.xpath(XPATH_DISLIKE_BUTTON)).size() > Utils.MAX_AMOUNT) { //max amount
                        System.out.println("we reached max count of linked posts. " +
                                "If you want more post to be linked, change max.count property in config.properties.");
                        endReached = true;
                        likedPostsTotal = findWebElements(By.xpath(XPATH_DISLIKE_BUTTON)).size();
                        continue;
                    }
                    //find all unliked posts
                    notLikedPosts = findWebElements(By.xpath(XPATH_LIKE_BUTTON)).stream()
                            .filter(e -> e.isDisplayed()).collect(Collectors.toList());
                    for (WebElement post : notLikedPosts) {
                        //scroll to and liked all unliked posts .//a[@aria-label='Отметьте как понравившееся']
                        scrollDownToElement(post);
                        post.sendKeys(Keys.UP);
                        post.sendKeys(Keys.UP);
                        post.sendKeys(Keys.UP);
                        if (post.isDisplayed() && post.isEnabled()) {
                            post.click();
                            likedPostsCount++;
//                        takeScreenshotElement(post, name + likedPostsCount);
                        }

                    }

                    //get all likked posts .//a[@aria-label='Не нравится']
                    if (likedPosts.size() > 0) {
                        likedPosts.get(likedPosts.size() - 1).sendKeys(Keys.PAGE_DOWN);
                    } else {
                        List<WebElement> dates = findWebElements(By.xpath(".//a[@rel='theater']/abbr[@data-utime]"));
                        dates.get(dates.size() - 1).sendKeys(Keys.PAGE_DOWN);
                    }
                    if (!Utils.checkAllPosts && findWebElements(By.xpath(XPATH_LIKE_BUTTON)).size() == 0) {
                        endReached = true;
                        likedPostsTotal = findWebElements(By.xpath(XPATH_DISLIKE_BUTTON)).size();
                    }
                }
                //log info
                takeScreenShot(name + likedPostsCount);
                if (Utils.checkAllPosts) {
                    usersStatus.put(name, MessageFormat.format("has liked {0} posts, total {1} posts}",
                            likedPostsCount, likedPostsTotal));
                } else {
                    usersStatus.put(name, MessageFormat.format("has liked {0} posts", likedPostsCount));
                }
                showStatus(usersStatus, name);
                findElement(By.xpath(".//*[@id='pageLoginAnchor']")).click();
                pause(1);
                findElement(By.xpath("//li[@data-gt='{\"ref\":\"async_menu\",\"logout_menu_click\":\"menu_logout\"}']")).click();
                pause(1);
            } else {
                usersStatus.put(name, "failed to login");
                showStatus(usersStatus, name);
            }
        });
        System.out.println();
        System.out.println("------------------------------------------------------------------------------------------");
        System.out.println("------------------------------------------------------------------------------------------");

    }

    private PrintStream showStatus(final Map<String, String> usersStatus, final String name) {
        return System.out.format("------------------------> (%s/%s) %s : %s \n", usersStatus.size(), Utils.users.size(), name, usersStatus.get(name));
    }

    private void takeScreenShot(String name) {
        File scrFile = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File("screenshots\\" + name + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
